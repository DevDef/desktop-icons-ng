# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# Eaglers <eaglersdeveloper@gmail.com>, 2018.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-12-31 15:09+0100\n"
"PO-Revision-Date: 2021-09-22 19:24+1000\n"
"Last-Translator: Ser82-png; Stas Solovey\n"
"Language-Team: \n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Poedit 2.3\n"

#: askRenamePopup.js:42
msgid "Folder name"
msgstr "Имя папки"

#: askRenamePopup.js:42
msgid "File name"
msgstr "Имя файла"

#: askRenamePopup.js:49 desktopManager.js:766
msgid "OK"
msgstr "ОК"

#: askRenamePopup.js:49
msgid "Rename"
msgstr "Переименовать"

#: desktopIconsUtil.js:88
msgid "Command not found"
msgstr "Команда не найдена"

#: desktopIconsUtil.js:225
msgid "Do you want to run “{0}”, or display its contents?"
msgstr "Вы хотите выполнить “{0}” или отобразить содержимое?"

#: desktopIconsUtil.js:226
msgid "“{0}” is an executable text file."
msgstr "“{0}” является исполняемым текстовым файлом."

#: desktopIconsUtil.js:230
msgid "Execute in a terminal"
msgstr "Выполнить в терминале"

#: desktopIconsUtil.js:232
msgid "Show"
msgstr "Показать"

#: desktopIconsUtil.js:234 desktopManager.js:768 fileItemMenu.js:338
msgid "Cancel"
msgstr "Отмена"

#: desktopIconsUtil.js:236
msgid "Execute"
msgstr "Выполнить"

#: desktopManager.js:198
msgid "Nautilus File Manager not found"
msgstr "Файловый менеджер Nautilus не найден"

#: desktopManager.js:199
msgid "The Nautilus File Manager is mandatory to work with Desktop Icons NG."
msgstr ""
"Файловый менеджер Nautilus является обязательным для работы с Desktop Icons "
"NG."

#: desktopManager.js:729
msgid "Clear Current Selection before New Search"
msgstr "Очистить текущую подборку перед новым поиском"

#: desktopManager.js:770
msgid "Find Files on Desktop"
msgstr "Поиск файлов на рабочем столе"

#: desktopManager.js:830 desktopManager.js:1473
msgid "New Folder"
msgstr "Создать папку"

#: desktopManager.js:834
msgid "New Document"
msgstr "Создать документ"

#: desktopManager.js:839
msgid "Paste"
msgstr "Вставить"

#: desktopManager.js:843
msgid "Undo"
msgstr "Отменить"

#: desktopManager.js:847
msgid "Redo"
msgstr "Повторить"

#: desktopManager.js:853
msgid "Select All"
msgstr "Выделить всё"

#: desktopManager.js:861
msgid "Show Desktop in Files"
msgstr "Показать рабочий стол в файловом менеджере"

#: desktopManager.js:865 fileItemMenu.js:267
msgid "Open in Terminal"
msgstr "Открыть в терминале"

#: desktopManager.js:871
msgid "Change Background…"
msgstr "Изменить фон…"

#: desktopManager.js:880
msgid "Display Settings"
msgstr "Настройки экрана"

#: desktopManager.js:887
msgid "Desktop Icons Settings"
msgstr "Параметры Desktop Icons"

#: desktopManager.js:1531
msgid "Arrange Icons"
msgstr "Упорядочить значки"

#: desktopManager.js:1535
msgid "Arrange By..."
msgstr "Упорядочить по..."

#: desktopManager.js:1544
msgid "Keep Arranged..."
msgstr "Сохранить порядок..."

#: desktopManager.js:1548
msgid "Keep Stacked by type..."
msgstr ""

#: desktopManager.js:1553
msgid "Sort Home/Drives/Trash..."
msgstr "Сортировать Домашняя папка/Диски/Корзина..."

#: desktopManager.js:1559
msgid "Sort by Name"
msgstr "Сортировать по имени"

#: desktopManager.js:1561
msgid "Sort by Name Descending"
msgstr "Сортировать по имени в порядке убывания"

#: desktopManager.js:1564
msgid "Sort by Modified Time"
msgstr "Сортировать по времени изменения"

#: desktopManager.js:1567
msgid "Sort by Type"
msgstr "Сортировать по типу"

#: desktopManager.js:1570
msgid "Sort by Size"
msgstr "Сортировать по размеру"

#. TRANSLATORS: "Home" is the text that will be shown in the user's personal folder
#: fileItem.js:136
msgid "Home"
msgstr "Домашняя папка"

#: fileItemMenu.js:105
msgid "Open All..."
msgstr "Открыть всё..."

#: fileItemMenu.js:105
msgid "Open"
msgstr "Открыть"

#: fileItemMenu.js:116
msgid "Stack This Type"
msgstr ""

#: fileItemMenu.js:116
msgid "Unstack This Type"
msgstr ""

#: fileItemMenu.js:128
msgid "Scripts"
msgstr "Сценарии"

#: fileItemMenu.js:134
msgid "Open All With Other Application..."
msgstr "Открыть всё в другом приложении..."

#: fileItemMenu.js:134
msgid "Open With Other Application"
msgstr "Открыть в другом приложении"

#: fileItemMenu.js:140
msgid "Launch using Dedicated Graphics Card"
msgstr "Запустить, используя дискретную видеокарту"

#: fileItemMenu.js:150
msgid "Cut"
msgstr "Вырезать"

#: fileItemMenu.js:155
msgid "Copy"
msgstr "Копировать"

#: fileItemMenu.js:161
msgid "Rename…"
msgstr "Переименовать…"

#: fileItemMenu.js:169
msgid "Move to Trash"
msgstr "Переместить в корзину"

#: fileItemMenu.js:175
msgid "Delete permanently"
msgstr "Удалить безвозвратно"

#: fileItemMenu.js:183
msgid "Don't Allow Launching"
msgstr "Не разрешать запуск"

#: fileItemMenu.js:183
msgid "Allow Launching"
msgstr "Разрешать запуск"

#: fileItemMenu.js:194
msgid "Empty Trash"
msgstr "Очистить корзину"

#: fileItemMenu.js:205
msgid "Eject"
msgstr "Извлечь"

#: fileItemMenu.js:211
msgid "Unmount"
msgstr "Отмонтировать"

#: fileItemMenu.js:221
msgid "Extract Here"
msgstr "Распаковать здесь"

#: fileItemMenu.js:225
msgid "Extract To..."
msgstr "Распаковать в..."

#: fileItemMenu.js:232
msgid "Send to..."
msgstr "Отправить в..."

#: fileItemMenu.js:238
msgid "Compress {0} file"
msgid_plural "Compress {0} files"
msgstr[0] "Сжать {0} файл"
msgstr[1] "Сжать {0} файла"
msgstr[2] "Сжать {0} файлов"

#: fileItemMenu.js:244
msgid "New Folder with {0} item"
msgid_plural "New Folder with {0} items"
msgstr[0] "Новая папка с {0} элементом"
msgstr[1] "Новая папка с {0} элементами"
msgstr[2] "Новая папка с {0} элементами"

#: fileItemMenu.js:253
msgid "Common Properties"
msgstr "Общие свойства"

#: fileItemMenu.js:253
msgid "Properties"
msgstr "Свойства"

#: fileItemMenu.js:260
msgid "Show All in Files"
msgstr "Показать всё в файловом менеджере"

#: fileItemMenu.js:260
msgid "Show in Files"
msgstr "Показать в файловом менеджере"

#: fileItemMenu.js:336
msgid "Select Extract Destination"
msgstr "Выбрать папку назначения для распаковки"

#: fileItemMenu.js:339
msgid "Select"
msgstr "Выделить"

#: fileItemMenu.js:366
msgid "Can not email a Directory"
msgstr "Невозможно отправить каталог по электронной почте"

#: fileItemMenu.js:367
msgid "Selection includes a Directory, compress the directory to a file first."
msgstr "Выбран каталог, сначала сожмите его в файл."

#: preferences.js:91
msgid "Settings"
msgstr "Параметры"

#: preferences.js:98
msgid "Size for the desktop icons"
msgstr "Размер значков на рабочем столе"

#: preferences.js:98
msgid "Tiny"
msgstr "Крошечный"

#: preferences.js:98
msgid "Small"
msgstr "Маленький"

#: preferences.js:98
msgid "Standard"
msgstr "Стандартный"

#: preferences.js:98
msgid "Large"
msgstr "Большой"

#: preferences.js:99
msgid "Show the personal folder in the desktop"
msgstr "Показывать домашнюю папку на рабочем столе"

#: preferences.js:100
msgid "Show the trash icon in the desktop"
msgstr "Показывать корзину на рабочем столе"

#: preferences.js:101 schemas/org.gnome.shell.extensions.ding.gschema.xml:45
msgid "Show external drives in the desktop"
msgstr "Показывать внешние диски на рабочем столе"

#: preferences.js:102 schemas/org.gnome.shell.extensions.ding.gschema.xml:50
msgid "Show network drives in the desktop"
msgstr "Показывать сетевые диски на рабочем столе"

#: preferences.js:105
msgid "New icons alignment"
msgstr "Выравнивание для новых значков"

#: preferences.js:106
msgid "Top-left corner"
msgstr "Верхний левый угол"

#: preferences.js:107
msgid "Top-right corner"
msgstr "Правый верхний угол"

#: preferences.js:108
msgid "Bottom-left corner"
msgstr "Нижний левый угол"

#: preferences.js:109
msgid "Bottom-right corner"
msgstr "Правый нижний угол"

#: preferences.js:111 schemas/org.gnome.shell.extensions.ding.gschema.xml:55
msgid "Add new drives to the opposite side of the screen"
msgstr "Добавлять новые диски на противоположную сторону экрана"

#: preferences.js:112
msgid "Highlight the drop place during Drag'n'Drop"
msgstr "Выделять место назначения во время Drag'n'Drop"

#: preferences.js:116
msgid "Settings shared with Nautilus"
msgstr "Общие настройки с Nautilus"

#: preferences.js:122
msgid "Click type for open files"
msgstr "Тип нажатия для открытия файлов"

#: preferences.js:122
msgid "Single click"
msgstr "Одинарный клик"

#: preferences.js:122
msgid "Double click"
msgstr "Двойной клик"

#: preferences.js:123
msgid "Show hidden files"
msgstr "Показывать скрытые файлы"

#: preferences.js:124
msgid "Show a context menu item to delete permanently"
msgstr "Добавить пункт в контекстное меню «Удалить безвозвратно»"

#: preferences.js:129
msgid "Action to do when launching a program from the desktop"
msgstr "Действие при запуске программы с рабочего стола"

#: preferences.js:130
msgid "Display the content of the file"
msgstr "Показать содержимое файла"

#: preferences.js:131
msgid "Launch the file"
msgstr "Запустить файл"

#: preferences.js:132
msgid "Ask what to do"
msgstr "Спросить что делать"

#: preferences.js:138
msgid "Show image thumbnails"
msgstr "Показывать миниатюры изображений"

#: preferences.js:139
msgid "Never"
msgstr "Никогда"

#: preferences.js:140
msgid "Local files only"
msgstr "Только для локальных файлов"

#: preferences.js:141
msgid "Always"
msgstr "Всегда"

#: prefs.js:37
msgid ""
"To configure Desktop Icons NG, do right-click in the desktop and choose the "
"last item: 'Desktop Icons settings'"
msgstr ""
"Чтобы настроить Desktop Icons NG, щелкните правой кнопкой мыши на рабочем "
"столе и выберите последний пункт: «Параметры Desktop Icons»."

#: showErrorPopup.js:37
msgid "Close"
msgstr "Закрыть"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:25
msgid "Icon size"
msgstr "Размер значка"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:26
msgid "Set the size for the desktop icons."
msgstr "Установить размер значков на рабочем столе."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:30
msgid "Show personal folder"
msgstr "Показывать домашнюю папку"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:31
msgid "Show the personal folder in the desktop."
msgstr "Показывать значок домашней папки на рабочем столе."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:35
msgid "Show trash icon"
msgstr "Показывать значок корзины"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:36
msgid "Show the trash icon in the desktop."
msgstr "Показывать значок корзины на рабочем столе."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:40
msgid "New icons start corner"
msgstr "Новые значки начинают размещаться с угла"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:41
msgid "Set the corner from where the icons will start to be placed."
msgstr "Установите угол, с которого значки начнут размещаться."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:46
msgid "Show the disk drives connected to the computer."
msgstr "Показывать диски, подключенные к компьютеру."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:51
msgid "Show mounted network volumes in the desktop."
msgstr "Показывать подключенные сетевые тома на рабочем столе."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:56
msgid ""
"When adding drives and volumes to the desktop, add them to the opposite side "
"of the screen."
msgstr ""
"Когда добавляются диски и тома на рабочий стол, располагать их на "
"противоположной стороне экрана."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:60
msgid "Shows a rectangle in the destination place during DnD"
msgstr "Показывать прямоугольник в месте назначения во время перемещения"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:61
msgid ""
"When doing a Drag'n'Drop operation, marks the place in the grid where the "
"icon will be put with a semitransparent rectangle."
msgstr ""
"При выполнении операции Drag'n'Drop отмечается место в сетке полупрозрачным "
"прямоугольником, куда будет помещён значок."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:65
msgid "Sort Special Folders - Home/Trash Drives."
msgstr "Сортировать специальный папки - Домашняя папка/Корзина/Диски."

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:66
msgid ""
"When arranging Icons on desktop, to sort and change the position of the "
"Home, Trash and mounted Network or External Drives"
msgstr ""
"При размещении значков на рабочем столе, для сортировки и изменения "
"положения Домашней папки, Корзины и подключенных сетевых или внешних дисков"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:70
msgid "Keep Icons Arranged"
msgstr "Сохранить порядок значков"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:71
msgid "Always keep Icons Arranged by the last arranged order"
msgstr "Всегда сохранять порядок значков используя последнее упорядочивание"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:75
msgid "Arrange Order"
msgstr "Упорядочивание значков"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:76
msgid "Icons Arranged by this property"
msgstr "Упорядочивание значков по этому свойству"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:80
#, fuzzy
msgid "Keep Icons Stacked"
msgstr "Сохранить порядок значков"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:81
#, fuzzy
msgid "Always keep Icons Stacked, Similar types are grouped"
msgstr "Всегда сохранять порядок значков используя последнее упорядочивание"

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:85
msgid "Type of Files to not Stack"
msgstr ""

#: schemas/org.gnome.shell.extensions.ding.gschema.xml:86
msgid "An Array of strings types, Dont Stack these types of files"
msgstr ""

#~ msgid "New folder"
#~ msgstr "Создать папку"

#~ msgid "Delete"
#~ msgstr "Удалить"

#~ msgid "Error while deleting files"
#~ msgstr "Ошибка при удалении файлов"

#~ msgid "Are you sure you want to permanently delete these items?"
#~ msgstr "Вы уверены, что хотите удалить эти файлы навсегда?"

#~ msgid "If you delete an item, it will be permanently lost."
#~ msgstr "Если вы удалите файл, он будет навсегда потерян."

#~ msgid ""
#~ "There was an error while trying to permanently delete the folder {:}."
#~ msgstr "Произошла ошибка при попытке окончательно удалить папку {:}."

#~ msgid "There was an error while trying to permanently delete the file {:}."
#~ msgstr "Произошла ошибка при попытке окончательно удалить файл {:}."

#, fuzzy
#~ msgid "Show external disk drives in the desktop"
#~ msgstr "Показывать домашнюю папку на рабочем столе"

#, fuzzy
#~ msgid "Show the external drives"
#~ msgstr "Показывать домашнюю папку на рабочем столе"

#, fuzzy
#~ msgid "Show network volumes"
#~ msgstr "Показать в «Файлах»"

#~ msgid "Enter file name…"
#~ msgstr "Ввести имя файла…"

#~ msgid "Huge"
#~ msgstr "Огромный"

#~ msgid "Ok"
#~ msgstr "ОК"
